
##Maven Module: protobuf-basic
```cd protobuf-basic```

Build proto files
```protoc -I=. --java_out=./src/main/java ./src/main/resources/addressbook.proto```

Run AddPerson
```mvn compile exec:java -Dexec.mainClass="AddPerson" -Dexec.args="person.txt"```

Run ListPeople
```mvn compile exec:java -Dexec.mainClass="ListPeople" -Dexec.args="person.txt"```

References:
https://developers.google.com/protocol-buffers/docs/javatutorial


##Maven Module: protobuf-basic

```cd protobuf-grpc```

References:
https://grpc.io/docs/tutorials/basic/java.html



